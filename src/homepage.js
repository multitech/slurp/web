import React from "react"
import {ImageStore} from "./resources/image-store"
import {MenuItem} from "./menu-item"

const socialIconWidth = "36px"

export class HomePage extends React.Component {
  render() {
    return (
      <div className="w-100 center white bg-brand-blue">
        {/* row 1 */}
        <div className="cf ph5-l flex-l items-center-l">
          <div className="fl-l w-50-l pa3 pt4 flex flex-inline items-center justify-center justify-start-l">
            <img
              alt="Kartbites logo"
              style={{height: "50px"}}
              className="mr3"
              {...ImageStore.get("wide-logo-final.png")}
            />
          </div>
          <div className="fl-l w-50-l pa3 pt4-l flex flex-inline items-center justify-center justify-end-l">
            <MenuItem className="ph3 faint-blue f4" href="https://kartbites.com">
              Home
            </MenuItem>
            <MenuItem
              className="ph3 faint-blue f4"
              href="https://medium.com/kartbites/tagged/street-food"
              target="_blank">
              Blog
            </MenuItem>
          </div>
        </div>
        {/* row 2 */}
        <div className="cf ph2-l mt5-l flex-l">
          <div className="fl-l w-60-l pa4 pb0-l pt1-l tc">
            <img alt="Pizza truck" {...ImageStore.get("pizza-truck.png")} />
          </div>
          <div className="fl-l w-40-l flex flex-column justify-end ph4 ph5-l pt4-l">
            <div className="measure-wide pr2-m pr2-l">
              <h1 className="f2 ma0 lh-title brand-orange tracked">
                Share your street food finds!
              </h1>
              <div>
                <p className="f4 lh-copy legible-grey">
                  How do we find ethenic street food sold out of anonymous hand-pulled food
                  trucks and carts?{" "}
                  <span role="img" aria-label="thinking face">
                    &#129300;
                  </span>{" "}
                  Well, with your help. All you have to do, is upload a picture. The location
                  tag in the picture helps us put street food on the map and make it
                  searchable.
                </p>
              </div>
            </div>
            <div className="inline-flex flex-row">
              <MenuItem
                href="https://play.google.com/store/apps/details?id=com.kartbites.mobileapp&hl=en_US&pcampaignid=MKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1"
                target="_blank"
                rel="noopener noreferrer">
                <img
                  alt="Get Kartbites for Android"
                  {...ImageStore.get("google-play-badge.png")}
                />
              </MenuItem>
              <MenuItem
                href="https://apps.apple.com/uy/app/kartbites/id1436784682"
                target="_blank"
                rel="noopener noreferrer">
                <img
                  alt="Get Kartbites for iOS"
                  {...ImageStore.get("appstore.png")}
                  className="pl2"
                />
              </MenuItem>
            </div>
            <div className="pt4">
              <img
                alt="Kartbites partial screen"
                style={{width: "350px"}}
                {...ImageStore.get("half-screenshot.png")}
              />
            </div>
          </div>
        </div>
        {/* row 3 */}
        <div className="cf flex-l items-center-l">
          <div className="fl-l w-50-l flex flex-column items-center">
            <div className="measure-narrow">
              <h2 className="f2 lh-title brand-orange tracked">Share Street Food Finds</h2>
              <p className="f4 lh-copy legible-grey">
                Simply take a picture of the food and publish it <b>anytime later</b>. You will
                see your street food discovery become searchable moments after you post. We
                will even award you the title of <b>&quot;explorer&quot;</b> if you are the
                first to find the street food cart, stand, or truck. Although we are all about
                street food, we welcome your restaurant contributions as well.
              </p>
            </div>
          </div>
          <div
            className="fl-l w-50-l pa4 pl5-l mt2 mt0-l ml2"
            style={{
              boxShadow: "0px 0px 30px 6px rgba( 0, 0, 0, 0.2 )",
              borderRadius: "50px 0px 0px 50px"
            }}>
            <img
              alt="Kartbites - publishing about street food"
              {...ImageStore.get("share.png")}
            />
          </div>
        </div>
        {/* row 4 */}
        <div className="cf flex-l items-center-l flex-row-reverse-l mt6-l">
          <div className="w-50-l flex flex-column items-center justify-center">
            <div className="measure-narrow">
              <h2 className="f2 lh-title brand-orange tracked">Find Street Food</h2>
              <p className="f4 lh-copy legible-grey">
                Use our search feature to find unique local restaurants, street food carts,
                stand, or trucks around a location. On any search result, press{" "}
                <b className="tracked">Been</b> or <b className="tracked">Try</b> and we will
                automatically add that place to your lists. There will be street food to search
                for only if someone had published about it in the past. Kartbites is a
                community powered app, all the street food information in it comes from our
                contributing members - the amazing foodies and street food enthusiasts that
                power the app.
              </p>
            </div>
          </div>
          <div className="w-50-l pa4 tc">
            <img
              alt="Kartbites - search for nearby street food"
              style={{width: "500px"}}
              {...ImageStore.get("discover.png")}
            />
          </div>
        </div>
        {/* lists row */}
        <div className="cf flex-l items-center-l mt6-l">
          <div className="w-50-l flex flex-column items-center">
            <div className="measure-narrow">
              <h2 className="f2 lh-title brand-orange tracked">Share Lists</h2>
              <p className="f4 lh-copy legible-grey">
                Anytime you publish about a street food cart, stand, or truck, we add that to
                your <b className="tracked">Been</b> list. Similar magic happens on pressing
                the <b className="tracked">Been</b> or <b className="tracked">Try</b> buttons.
                Now you&#39;ll always know where you&#39;ve been and what you want to try. And
                did we mention you can share these lists with anyone? Yes. Anyone.
              </p>
            </div>
          </div>
          <div className="w-50-l pa4 tc">
            <img
              alt="Kartbites - been and try lists"
              style={{width: "300px"}}
              {...ImageStore.get("remember.png")}
            />
          </div>
        </div>
        {/* socialize row */}
        <div className="cf flex-l items-center-l flex-row-reverse-l mt6-l">
          <div className="w-50-l flex flex-column items-center">
            <div className="measure-narrow">
              <h2 className="f2 lh-title brand-orange tracked">Socialize</h2>
              <p className="f4 lh-copy legible-grey">
                Kartbites is nothing without its contributing members.{" "}
                <b className="b tracked">We. Love. You</b>. Join in by following friends or
                foodies that are already here, comment on their posts, and share your lists.
                Its the best way to quickly discover new street food or restaurants and make
                some new friends.
              </p>
            </div>
          </div>
          <div className="w-50-l pa4 tc">
            <img
              alt="Kartbites social network"
              style={{width: "500px"}}
              {...ImageStore.get("socialize.png")}
            />
          </div>
        </div>
        {/* lists row */}
        <div className="cf flex-l items-center-l mt6-l">
          <div className="w-50-l flex flex-column items-center">
            <div className="measure-narrow">
              <h2 className="f2 lh-title brand-orange tracked">Track Street Food Stats</h2>
              <p className="f4 lh-copy legible-grey">
                Track how much you have posted, how many restaurants and street food joints you
                have discovered (thank you!), how many you have been to, and how many you want
                to try. We hope this inspires you to share more!
              </p>
            </div>
          </div>
          <div className="w-50-l pa4 tc">
            <img
              alt="Kartbites - track your stats"
              style={{width: "350px"}}
              {...ImageStore.get("track.png")}
            />
          </div>
        </div>
        {/* conclusion */}
        <div className="cf flex-l flex-inline-l items-center-l bt b--faint-blue pv4 mh4 mt6-l">
          <div className="tracking f5 fl-l w-50-l legible-grey pl3-l">
            We can tell you like this app, now go get it from the stores!
          </div>
          <div className="fl-l w-50-l flex inline-flex mt4 mt0-l justify-end-l pr3-l">
            <MenuItem
              href="https://play.google.com/store/apps/details?id=com.kartbites.mobileapp"
              target="_blank"
              rel="noopener noreferrer">
              <img
                alt="Get Kartbites for Android"
                {...ImageStore.get("google-play-badge.png")}
              />
            </MenuItem>
            <MenuItem
              href="https://apps.apple.com/uy/app/kartbites/id1436784682"
              target="_blank"
              rel="noopener noreferrer">
              <img
                alt="Get Kartbites for iOS"
                {...ImageStore.get("appstore.png")}
                className="pl2"
              />
            </MenuItem>
          </div>
        </div>
        {/* footer */}
        <div className="cf flex-l flex-inline-l mh4 pb3 pb4-l bt b--faint-blue">
          <div className="fl-l w-40-l pl3-l pt4">
            <img
              alt="Kartbites logo"
              className="h2 mr2"
              {...ImageStore.get("wide-logo-final.png")}
            />
          </div>
          <div className="fl-l w-20-l flex flex-column items-start mt4">
            <MenuItem href="https://kartbites.com">
              <h5 className="ma0 legible-grey">Home</h5>
            </MenuItem>
            <MenuItem
              className="mt2"
              href="https://medium.com/kartbites/tagged/street-food"
              target="_blank">
              <h5 className="ma0 legible-grey">Blog</h5>
            </MenuItem>
            <MenuItem className="mt2" href="./privacy.pdf" target="_blank">
              <h5 className="ma0 legible-grey">Privacy Policy</h5>
            </MenuItem>
            <MenuItem className="mt2" href="./touse.pdf" target="_blank">
              <h5 className="ma0 legible-grey">Terms and Conditions</h5>
            </MenuItem>
            <MenuItem className="mt2" href="mailto:contact@kartbites.com">
              <h5 className="ma0 legible-grey">Contact</h5>
            </MenuItem>
          </div>
          <div className="fl-l w-40-l flex-l flex-column-l justify-between-l items-end-l mt4">
            <div className="flex flex-inline items-center mb4 pr2">
              <div className="f7 lh-copy tracked legible-grey normal measure-wide-l tr-l">
                Merlin Infinite, DN 51, Unit 714, Sector V Kolkata, West Bengal 700091, India
              </div>
              <img
                alt="Location pin"
                className="ph2"
                style={{width: "15px", height: "20px"}}
                {...ImageStore.get("locpin.png")}
              />
            </div>
          </div>
        </div>
        {/* copyright, social */}
        <div className="cf flex-l flex-inline-l items-center-l flex-row-reverse-l mh4 pb4">
          <div className="fl-l w-50-l flex flex-inline justify-end-l mr3-l">
            <MenuItem
              href="https://www.instagram.com/kartbites_app/"
              target="_blank"
              rel="noopener noreferrer">
              <img
                alt="Kartbites on Instagram"
                className="ml3"
                style={{width: socialIconWidth, height: socialIconWidth}}
                {...ImageStore.get("instagram-round.png")}
              />
            </MenuItem>
            <MenuItem
              href="https://www.facebook.com/Kartbites/"
              target="_blank"
              rel="noopener noreferrer">
              <img
                alt="Kartbites on Facebook"
                className="ml3"
                style={{width: socialIconWidth, height: socialIconWidth}}
                {...ImageStore.get("fb-round.png")}
              />
            </MenuItem>
            <MenuItem
              href="https://twitter.com/kartbites"
              target="_blank"
              rel="noopener noreferrer">
              <img
                alt="Kartbites on Twitter"
                className="ml3"
                style={{width: socialIconWidth, height: socialIconWidth}}
                {...ImageStore.get("twitter-round.png")}
              />
            </MenuItem>
            <MenuItem
              href="https://www.linkedin.com/company/kartbites/"
              target="_blank"
              rel="noopener noreferrer">
              <img
                alt="Kartbites on LinkedIn"
                className="ml3"
                style={{width: socialIconWidth, height: socialIconWidth}}
                {...ImageStore.get("linkedin-round.png")}
              />
            </MenuItem>
            <MenuItem
              href="https://medium.com/kartbites/tagged/street-food"
              target="_blank"
              rel="noopener noreferrer">
              <img
                alt="Kartbites on Medium"
                className="ml3"
                style={{width: socialIconWidth, height: socialIconWidth}}
                {...ImageStore.get("medium-round.png")}
              />
            </MenuItem>
          </div>
          <h6 className="ma0 tracked fl-l w-50-l f7 tracked legible-grey tl-l mt3 mt0-l ml3-l">
            Copyright &copy; Kartbites {new Date().getFullYear()}. All rights reserved.
          </h6>
        </div>
      </div>
    )
  }
}
