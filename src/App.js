import React from "react"
import {HomePage} from "./homepage"

class App extends React.Component {
  render() {
    return <HomePage />
  }
}

export default App
