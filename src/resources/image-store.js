const maxPixelRatio = 3 // 1x 2x 3x

export class ImageStore {
  static get(name) {
    let src = require("./" + name)

    let pixelRatio =
      window.devicePixelRatio > maxPixelRatio
        ? maxPixelRatio
        : Math.round(window.devicePixelRatio)

    let srcSet = ""

    if (pixelRatio > 1) {
      let densityMatchedImageName = ""
      try {
        let nameParts = name.split(".")
        nameParts.splice(
          nameParts.length - 2,
          1,
          nameParts[nameParts.length - 2] + "@" + pixelRatio + "x"
        )
        densityMatchedImageName = nameParts.join(".")
        // srcset is an HTML attribute for the image tag.
        // it allows selecting images based on screen density
        // so that you show the clearest image possible on
        // every screen.
        srcSet = require("./" + densityMatchedImageName) + " " + pixelRatio + "x"
      } catch (e) {
        console.error(
          "No matching image for display pixel ratio",
          pixelRatio,
          name,
          densityMatchedImageName,
          e
        )
      }
    }

    return {
      src,
      srcSet
    }
  }
}
