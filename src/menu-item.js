import React from "react"

const defautStyles = "link dim tracked "

// This is an anchor element so pass it any
// anchor props you want.
export class MenuItem extends React.Component {
  render() {
    let styleClasses = defautStyles
    if (this.props.className) {
      styleClasses += this.props.className
    }

    return (
      <a {...this.props} className={styleClasses}>
        {this.props.children}
      </a>
    )
  }
}
